const messaging = require('../src/messaging');

const createRedisClient = require('../src/createRedisClient');
jest.mock('../src/createRedisClient');

// The stream used in testing
// Redis is mocked so it will not actually be written to
const stream = 'test-stream';

test('Test sending a message', async (done) => {
  // Create test values
  const message = {
    hello: 'world',
    key: 'value',
    other: 'key value pair',
    notThere: undefined,
  };

  // Mock the redis client
  createRedisClient.mockReturnValue({
    xadd: (addingStream, id, ...rest) => {
      // Check the stream and id
      expect(addingStream).toBe(stream);
      expect(id).toBe('*');

      // Get the values
      const vals = rest.splice(0, rest.length - 1);

      // Make sure undefined was not added
      expect(vals.includes(undefined) || vals.includes('notThere')).toBe(false);

      // Check if all the keys are present and the values are correct
      for (const field in message) {
        if (message.hasOwnProperty(field) && message[field] !== undefined) {
          const idx = vals.indexOf(field);
          expect(idx).not.toBe(-1);
          expect((vals[idx + 1] = JSON.stringify(message[field])));
        }
      }

      // Call the callback
      rest[0]();
    },
  });

  await messaging.sendMessage(stream, message);
  done();
});

test('Test receiving messages', async (done) => {
  // Set test environment variables
  process.env.REDIS_CONSUMER_GROUP = 'some-consumer-group';
  process.env.REDIS_CONSUMER_ID = 'some-consumer-id';

  // Create test values as they will be received from Redis
  const messages = [
    [['key', '"value"', 'otherKey', '"value2"', 'oneMoreKey', '2'], 0],
    null,
    null,
    [['messageNum', '2', 'someString', '"string here"'], 1],
  ];

  // Mock the redis client
  let messageCounter = 0;
  const unackedMessages = [];
  createRedisClient.mockReturnValue({
    xreadgroup: (...args) => {
      // Remove the callback
      args.splice(args.length - 1, 1);

      expect(args).toEqual([
        'GROUP',
        process.env.REDIS_CONSUMER_GROUP,
        process.env.REDIS_CONSUMER_ID,
        'COUNT',
        1,
        'BLOCK',
        1000,
        'STREAMS',
        stream,
        '>',
      ]);

      if (messageCounter > messages.length) {
        // Return the messages while we still have some
        // Add message to unacked queue
        unackedMessages.push(messages[messageCounter][1]);

        let ret;
        if (messages[messageCounter] !== null)
          ret = [[null, [messages[messageCounter]]]];
        else {
          ret = null;
        }
        messageCounter++;

        return ret;
      } else {
        // No more messages
        // Make sure all messages have been acked and exit
        expect(unackedMessages.length).toBe(0);
        done();
        return null;
      }
    },
    xack: (id) => {
      unackedMessages.splice(unackedMessages.indexOf(id), 1);
    },
  });

  messaging.subscribeStream(stream, (message, id) => {
    // Craft the expected message and check if it matches the received message
    const expectedMessageArr = messages[messageCounter - 1][0];
    const expectedMessage = {};
    for (let idx = 0; idx < expectedMessageArr.length; idx += 2) {
      expectedMessage[expectedMessageArr[idx]] = JSON.encode(
        expectedMessageArr[idx + 1]
      );
    }
    expect(message).toBe(expectedMessage);

    // Check if the id matches
    if (expectedMessage) expect(id).toBe(messages[messageCounter - 1][1]);
  });
});
