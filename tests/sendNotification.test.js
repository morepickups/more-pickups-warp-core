const sendNotification = require('../src/sendNotification');

const messaging = require('../src/messaging');
jest.mock('../src/messaging');

test('Test sending notifications', async (done) => {
  // Create test values
  const notification = {
    user: {
      id: 1,
    },
    email: {
      subject: 'The email subject',
      html: "The email's html body",
      text: "The email's text body",
    },
    sms: 'The sms body',
    channel: 'email',
  };

  // Mock messaging
  messaging.sendMessage.mockImplementation((stream, message) => {
    expect(stream).toEqual('notifications');
    expect(message).toEqual({
      user: notification.user.id,
      subject: notification.email.subject,
      body: notification.email.html,
      textBody: notification.email.text,
      sms: notification.sms,
      channel: notification.channel,
    });
  });

  await sendNotification(notification);
  done();
});
