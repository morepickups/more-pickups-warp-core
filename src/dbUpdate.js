const util = require('util');

const messaging = require('./messaging');

/**
 * This handles syncing the db with the redis streams. When a new row is found on the stream,
 * if the id of the row exists in the database the row is updated, otherwise a new row is created.
 *
 * @param {Array} models an array of the models to be updated from the redis stream
 * @return {Promise} a promise which never resolves
 */
async function dbUpdate(models) {
  return Promise.all(models.map(updateModel));
}

/**
 * Handle updating one table.
 *
 * @param model the model to sync
 * @return {Promise<void>} a promise which never resolves
 */
async function updateModel(model) {
  return messaging.subscribeStream(model.getTableName(), async (rowObj) => {
    // Check if the row exists
    const row = await model.findByPk(rowObj.id);
    if (row) {
      // Update the existing row
      await row.update(rowObj);
    } else {
      // Create a new row
      await model.create(rowObj);
    }
  });
}

module.exports = dbUpdate;
