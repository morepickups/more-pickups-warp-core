const passport = require('passport');

module.exports = {
  /**
   * Initialize passport and register the middleware required for authentication to function.
   *
   * @param app the express app
   * @param users the users model
   */
  initialize: (app, users) => {
    // Setup user serialization and deserialization
    passport.serializeUser((user, done) => {
      // Serialize a user by getting it's id
      done(null, user.id);
    });
    passport.deserializeUser(async (id, done) => {
      try {
        // Deserialize the user by finding the user with the right id
        done(null, await users.findByPk(id));
      } catch (ex) {
        // Handle any exceptions
        done(ex);
      }
    });

    // Register middleware
    app.use(passport.initialize());
    app.use(passport.session());
  },
  /**
   * Authentication middlewares
   */
  middleware: {
    /**
     * Require login to access route. Returns 401 (Unauthorized) if user is not logged in.
     */
    requireLogin: (req, res, next) => {
      if (req.user) {
        next();
        return;
      }

      res.sendStatus(401);
    },
  },
};
