const redis = require('redis');

/**
 * Get a redis client using the host and port in the env vars REDIS_HOST
 * and REDIS_PORT environment variables respectively. This function will
 * continuously retries every 100 ms until it is able to create a client.
 *
 * @returns {RedisClient}
 */
module.exports = () =>
  redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    retry_strategy: () => 100,
  });
