const util = require('util');

const createRedisClient = require('./createRedisClient');

module.exports = {
  /**
   * Send a message on a stream.
   *
   * @param {String} stream the stream to append the message to
   * @param {Object} message the message to append to the stream
   * @returns {Promise<void>}
   */
  sendMessage: async (stream, message) => {
    const redisClient = createRedisClient();
    const xadd = util.promisify(redisClient.xadd).bind(redisClient);

    let args = [stream, '*'];

    for (const field in message) {
      if (message.hasOwnProperty(field))
        if (message[field] !== undefined)
          args.push(field, JSON.stringify(message[field]));
    }

    await xadd(...args);
  },
  /**
   * Subscribe to messages on a given stream.
   *
   * @param {String} stream the stream to subscribe to
   * @param {function(Object, String)} handler the callback to handle a message;
   *                                   passed the message and id and may return a promise
   * @return {Promise<void>} a promise which never resolves
   */
  subscribeStream: async (stream, handler) => {
    const redisClient = createRedisClient();
    const xreadgroup = util.promisify(redisClient.xreadgroup).bind(redisClient);
    const xack = util.promisify(redisClient.xack).bind(redisClient);

    while (true) {
      // Get a message from Redis
      const messageData = await xreadgroup(
        'GROUP',
        process.env.REDIS_CONSUMER_GROUP,
        process.env.REDIS_CONSUMER_ID,
        'COUNT',
        1,
        'BLOCK',
        1000,
        'STREAMS',
        stream,
        '>'
      );

      // Try again if no message was found
      if (messageData === null) continue;

      // Convert the message data into an object
      const messageArr = messageData[0][1][0][1];
      const messageRedisId = messageData[0][1][0][0];
      const messageObj = {};
      for (let idx = 0; idx < messageArr.length; idx += 2) {
        messageObj[messageArr[idx]] = JSON.parse(messageArr[idx + 1]);
      }

      // Call the handler to process the message
      await handler(messageObj, messageRedisId);

      // Tell Redis we've processed the message
      await xack(stream, process.env.REDIS_CONSUMER_GROUP, messageRedisId);
    }
  },
};
