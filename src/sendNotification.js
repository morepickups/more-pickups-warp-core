const util = require('util');

const messaging = require('./messaging');

/**
 * Adds a notification to the notifications stream.
 *
 * @param {Object} options
 * @param {number} options.user.id the id of the user to notify
 * @param {string} options.email.subject the notification email's subject
 * @param {string} options.email.html the notification email's html body
 * @param {string} options.email.text the notification email's text body
 * @param {string} options.sms the notification SMS body
 * @param {string} options.channel override the user's default notification channel
 * @returns {Promise<void>}
 */
module.exports = async (options) => {
  await messaging.sendMessage('notifications', {
    user: options.user.id,
    subject: options.email.subject,
    body: options.email.html,
    textBody: options.email.text,
    sms: options.sms,
    channel: options.channel,
  });
};
